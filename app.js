const express = require('express')
const { getDb, connectToDb } = require('./db')
const { ObjectId } = require('mongodb')

// init app & middleware
const app = express()
app.use(express.json())

// db connection
let db

connectToDb((err) => {
  if (!err) {
    app.listen('3000', () => {
      console.log('app listening on port 3000')
    })
    db = getDb()
  }
})

// routes
app.get('/guests', (req, res) => {
  let books = []

  db.collection('guests')
    .find()
    .sort({ author: 1 })
    .forEach(book => books.push(book))
    .then(() => {
      res.status(200).json(books)
    })
    .catch(() => {
      res.status(500).json({ error: 'Could not fetch the documents' })
    })
})

app.get('/guests/:id', (req, res) => {

  if (ObjectId.isValid(req.params.id)) {

    db.collection('guests')
      .findOne({ _id: new ObjectId(req.params.id) })
      .then(doc => {
        res.status(200).json(doc)
      })
      .catch(err => {
        res.status(500).json({ error: 'Could not fetch the document' })
      })

  } else {
    res.status(500).json({ error: 'Could not fetch the document' })
  }

})

app.post('/guests', (req, res) => {
  const guest = req.body

  db.collection('guests')
    .insertOne(guest)
    .then(result => {
      res.status(201).json(result)
    })
    .catch(err => {
      res.status(500).json({ err: 'Could not create a new document' })
    })
})

app.delete('/guests/:id', (req, res) => {

  if (ObjectId.isValid(req.params.id)) {

  db.collection('guests')
    .deleteOne({ _id: new ObjectId(req.params.id) })
    .then(result => {
      res.status(200).json(result)
    })
    .catch(err => {
      res.status(500).json({error: 'Could not delete document'})
    })

  } else {
    res.status(500).json({error: 'Could not delete document'})
  }
})

app.patch('/guests/:id', (req, res) => {
  const updates = req.body

  if (ObjectId.isValid(req.params.id)) {

    db.collection('guests')
      .deleteOne({ _id: new ObjectId(req.params.id) }, {$set: updates})
      .then(result => {
        res.status(200).json(result)
      })
      .catch(err => {
        res.status(500).json({error: 'Could not update document'})
      })
  
    } else {
      res.status(500).json({error: 'Not a valid doc id'})
    }

})